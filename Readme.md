# OpenGL Renderer
This is a opengl based renderer for rendering training data by giving it 3d Models. It creates a isohedron around the Model and captures it from evenly spaced angles. It can currently render RGB and Depth images. For RGB there is a basic silhuette shader and also a bit advanced shader that computes the shades by calculating the normals.

This code was adapted from https://github.com/aelmiger/LINE-MOD-Pipeline
## Prerequisites
To build the renderer, following libraries are required
- Opencv 4
- SDL2
- glm
- assimp
- glew
This was tested on Ubuntu 20.04 focal with linux kernel Linux 5.13.0-52
To install the dependencies following command can be used 
```
sudo apt-get install libglew-dev libglm-dev libassimp-dev libsdl2-dev`
```
## Supported 3D formats
The renderer uses Assimp to load the 3D files, so every fileending supported by Assimp should work. Those are listed [here](https://github.com/assimp/assimp/blob/master/doc/Fileformats.md)

I personally only tested ply and obj files, so go with those if possible.
## Building and running
Currently render_test.cpp loads a model and renders the RGB images of it. Rendering Depth can be enabled by including the calls to `renderDepthToFrontBuff` in the `renderImages` Function. 

To build it, create a build folder, run cmake and make the project:
``` 
make build
cd build
cmake ..
make -j
```

Because of the shader location, the program needs to be run from the root of the workspace. If you have a blank screen, then check if the shaders give you an error in the start. This is probably because you are trying to run it from the build folder and not from the root. 
## Shading 
For the RGB images there are two types of shaders. To change them, the correct shader file has to be set in `OpenglRenderer.cpp` line 252 and 266.

The `basic` shader renders a silhuette of the 3D models. This results in nice edges, but does not show any details of the Model

The `shading` shader implements a basic diffuse lighting shader, that sets a light source in front of the model. The position and intensity of that can be set in `shading.fs`. This could be extended to a phong shader if needed. For more information see https://learnopengl.com/Lighting/Basic-Lighting 
