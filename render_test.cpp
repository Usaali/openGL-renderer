#include <vector>
#include "defines.h"
#include "utility.h"
#include "OpenglRender.h"
#include "CameraViewPoints.h"

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <opencv2/highgui.hpp>

void renderImages(OpenGLRender* opengl, std::vector<cv::Mat>& in_imgVec, std::vector<glm::vec3> camVertices, uint16_t in_modelIterator,
                                     uint16_t in_vertIterator)
{
    //Für Tiefenbilder müssen die auskommentierten Zeilen wieder einkommentiert werden
	in_imgVec.clear();
	// opengl->renderDepthToFrontBuff(in_modelIterator, glm::vec3(camVertices[in_vertIterator]));
	// cv::Mat depth = opengl->getDepthImgFromBuff();
	opengl->renderColorToFrontBuff(in_modelIterator, glm::vec3(camVertices[in_vertIterator]), glm::vec3(0,0,0));
	cv::Mat color = opengl->getColorImgFromBuff();
	std::vector<cv::Mat> images;
	in_imgVec.push_back(color);
	//in_imgVec.push_back(depth);
}

int main() {
    //setup camera params for renderer
    CameraParameters camParams = {.fx = 910.995, .fy = 910.704,
                                  .cx = 962.918, .cy = 594.085,
                                  .videoWidth = 1280, .videoHeight = 720,
                                   };
    double camMat[3][3] = {{camParams.fx, 0, camParams.cx}, {0, camParams.fy, camParams.cy}, {0, 0, 1}}; //create camera matrix
    camParams.cameraMatrix = cv::Mat(3,3,CV_64FC1, &camMat);
    camParams.distortionCoefficients = cv::Mat1d(1,5); //distortion coeffs all set to zero 

    OpenGLRender* opengl = new OpenGLRender(camParams);
    CameraViewPoints* camPoints = new CameraViewPoints();

    std::string modelPath = "/your/path/to/model.ply"; //put your model path here!!!

    opengl->creatModBuffFromFiles(modelPath, glm::vec3(1,0,0)); //read model and define its color (color in float values. so (1,0,0) is red)
    for(int rad = 300; rad< 400; rad+= 100){
        camPoints->createCameraViewPoints(rad, 2); //create the viewpoints. Finer steps can be chosen by setting in_subdivisions higher
        std::vector<glm::vec3> camVertices = camPoints->getVertices();
        std::size_t numCameraVertices = camVertices.size();

        for(size_t j=0; j < numCameraVertices; j++) { //step through every vertice
            std::vector<cv::Mat> images;
            renderImages(opengl, images, camVertices, 0, j); //render images and save them into vector
            
            // double min, max;
            // cv::minMaxIdx(images[1], &min, &max, NULL, NULL, images[1]>0);
            // std::cout << min <<", " << max << std::endl;
            // images[1].setTo(min, images[1]==0);
            // cv::minMaxIdx(images[1], &min, &max);
            // //std::cout << min <<", " << max << std::endl;
            // cv::Mat depth;
            // images[1].convertTo(depth, CV_8U, 255 / (max-min), -255*min/(max-min));
            // cv::Mat colDepth;
            // cv::applyColorMap(depth, colDepth, cv::COLORMAP_AUTUMN);
            //cv::imshow("Depth", colDepth);
            //cv::imshow("Raw Depth", images[1]);
            std::cout << j << "\t/ "<< numCameraVertices << std::endl;
            
            cv::waitKey(10);
        }
    }
    //cleanup
    delete opengl;
    delete camPoints;
    return 0;
}
