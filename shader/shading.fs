//FRAGMENT SHADER
#version 330 core

layout(location = 0) out vec4 f_color;


in vec3 v_color;
in vec3 v_normal;
in vec3 frag_pos;

void main()
{
    vec3 norm = normalize(v_normal);
    vec3 lightDir = normalize(vec3(10.0f,10.0f,0.0f));
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * vec3(1.0f, 1.0f, 1.0f); //diffuse lighting has white color
    vec3 result = (diffuse + vec3(0.3f, 0.3f, 0.3f)) * v_color; //diffuse + ambient. If you want to make everything brighter, increase the ambient values from 0.3f
    f_color = vec4(result,1.0f);
}