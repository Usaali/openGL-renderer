#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_color;
layout(location = 2) in vec3 a_normal;

uniform mat4 u_modelViewProj;
out vec3 v_color;
out vec3 v_normal;
out vec3 frag_pos;

void main()
{
    gl_Position = u_modelViewProj * vec4(a_position, 1.0f);
	v_color = a_color;
    v_normal = a_normal;
    frag_pos = a_position;
}